# Memory Match Game

This repository contains the code for a simple Memory Match game implemented using HTML, CSS, and JavaScript.

## Files


1. memoryMatch.css: This file contains the CSS styling for the game interface, defining the appearance of elements such as the game board, cards, messages, and headings.
   
2. memoryMatch.html: This HTML file defines the structure of the game interface. It includes elements such as the game title, messages container, game board, and individual cards.

3. memoryMatch.js: This JavaScript file contains the logic for the Memory Match game. It handles card flipping, matching, and game completion logic.

## Usage

To play the Memory Match game:

1. Clone the repository to your local machine.
2. Open the `memoryMatch.html` file in a web browser.
3. Click on the cards to reveal their content.
4. Match pairs of cards with identical content to win the game.

## Features

- Random shuffling of card positions at the beginning of each game.
- Dynamic content generation for each card.
- Match detection and feedback messages.
- Game completion detection.

## Dependencies

- jQuery: This game utilizes jQuery for DOM manipulation and event handling.

## Additional Information

The game board consists of 36 cards (6 rows and 6 columns).
Each card contains a number ranging from 1 to 18, and there are two cards with each number.
The game ends when all pairs of cards are matched.
If two flipped cards do not match, they are flipped back after a brief delay.


## Credits

This readme file was created by me
